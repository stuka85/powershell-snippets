﻿
#Checks given list of servers for print spooler status. Useful in Print Nightmare vulnerability mitigation efforts.

$servers = Get-Content C:\Temp\serverList.txt

foreach ($server in $servers){
    Get-Service -ComputerName $server -Name spooler | Select @{Name="Server";Expression={"$server"}},Status,Name,DisplayName
}