﻿

#Finds all pdf files in a directory branch and all subbranches under, zips them into a file and deletes them.
#Used to reduce the total time needed to robocopy a file server between different locations.
#Could work with different extensions other than .pdf. Untested because of file size limitations.

$root = Read-Host "Enter root directory"

$name = 0

$compress = @{
    Path = $root + "\*.pdf"
    CompressionLevel = "Fastest"
    DestinationPath = $root + "\comp" + $name + ".zip"
}
Compress-Archive @compress
Remove-Item $root\*.pdf

$name++

$tocomp = Get-ChildItem -Recurse -Directory -Path $root

foreach ($path in $tocomp) {

    $compress = @{
            
        Path = $path.FullName + "\*.pdf"
        CompressionLevel = "Fastest"
        DestinationPath = $path.FullName + "\comp" + $name + ".zip"
        
    }
    Compress-Archive @compress
    $toRemove = $path.FullName + '\*.pdf'
    Remove-Item $toRemove
    $name++
}