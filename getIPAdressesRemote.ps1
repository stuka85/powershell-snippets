﻿
#Obtains IP addresses from a given list of computers and outputs it to a csv file.


import-module activedirectory
$list = Get-Content C:\Temp\Computer-list.txt
foreach ($i in $list) {
    Resolve-DnsName -Name $i -Type A |
    Select Name,IPAddress |
    Out-File C:\Temp\clients.csv -Append
}