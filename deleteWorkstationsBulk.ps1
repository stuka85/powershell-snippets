﻿
#Deletes computers from AD in bulk.

Import-Module ActiveDirectory

$computers = Get-Content C:\Temp\delete.csv

foreach ($pc in $computers) {
    Get-ADComputer -Identity $pc | Remove-ADObject -Recursive -Confirm:$False

}


