﻿
#Outputs all users included in a given AD group in a csv file.

import-module Activedirectory
$group = Read-Host "Enter group name"
Get-ADGroupMember -Identity $group | Export-Csv -Path c:\temp\users.csv -NoTypeInformation
