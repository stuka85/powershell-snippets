﻿#Removes a list of users from a provided group.

Import-Module ActiveDirectory

$group = Read-Host "Enter group name"

$users = Get-Content -Path 'C:\Temp\users.txt'

foreach ($user in $users){
   Remove-ADGroupMember $group -Members $user -Confirm:$false
}
