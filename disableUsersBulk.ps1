﻿

#Disables users in bulk.

Import-Module ActiveDirectory

$disable = Import-Csv -Path C:\Temp\disable.csv

foreach ($user in $disable) {
    Disable-ADAccount -Identity $user -Confirm false
}


