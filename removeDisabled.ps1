﻿
#Removes all disabled users from a given AD group.


Import-Module ActiveDirectory

$group = Read-Host "Enter group name"
$users = Get-AdGroupMember -Identity $group | Get-ADUser | Where -Property Enabled -eq $false

foreach ($user in $users){
    Remove-ADGroupMember -Identity $group -Members $users
}
