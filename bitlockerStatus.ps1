﻿
#Checks filtered computers in AD and returns last logon and last time Bitlocker password was set.

Import-module ActiveDirectory

function exportList {


#Adjust name filter as necessary

    $Computers = Get-ADComputer -Filter "name -like '*'" -Properties LastLogonDate
    foreach ($Computer in $Computers){

        $bitlockerPass = Get-AdObject -Filter "objectClass -eq 'msFVE-RecoveryInformation'" -SearchBase $computer.DistinguishedName -Properties msFVE-RecoveryPassword,whenCreated | Sort whenCreated -Descending | Select -First 1 | Select -ExpandProperty whenCreated

        $out = New-Object psobject
        $out | add-member noteproperty Name $computer.Name
        $out | add-member noteproperty LastLogonDate $computer.LastLogonDate
        $out | add-member noteproperty BitLockerPasswordSet $bitlockerPass
        $out
    }
}
exportList | Export-Csv -Path C:\temp\bitlocker.csv