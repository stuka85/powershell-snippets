﻿#Moves computers from a list in bulk

import-module ActiveDirectory
$computers = Get-Content C:\Temp\Computer-list.csv
$target = Read-Host "Enter target location"

foreach ($computer in $computers) {
Get-ADComputer $computer | Move-ADObject -TargetPath $target }
